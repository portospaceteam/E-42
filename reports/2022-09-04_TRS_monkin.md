## SITUATION REPORT 2 (03/09/2022)

# Comparison between different TRS Receiver Systems

## Introduction

The aim of this report is to analyze and compare existing Total Recovery Systems (TRS), in order to know the requirements of such a system and help us taking important technical decisions, such as the software's architecture and tech stack.

Unfortunately, I wasn't able to find many projects with source code that fitted exactly our own. However, I've narrowed it down to 3 that are very similar, namely:

1. Rocket Tracker <sup>1</sup> - Model Rocket tracking system
2. White Vest Model Rocket <sup>2</sup> - A more complete model rocket tracking system
3. Vehicle Tracking System <sup>3</sup> - Vehicle tracking system

Since there isn't a decision yet made regarding the radio transceiver that will be used, I've included examples with both LoRa and XBee modules.

## 1 - Rocket Tracker

This tracking system objective is to find a model rocket via GPS and display its location in a map, via a client app. To have an idea of the structure and the relationship between its components, I've made the following simplified diagram:
![Rocket Tracker's components diagram](assets/COTS/componentsDiagram1.png)
Please note that the arrows in the diagram represent a dependency association.

Since this report's main focus is on the receiver system, I will not give many details about the transmitter. Anyhow, it is important to know that the radio system is an **XBee radio** in both the sender and receiver. Furthermore, the transmission packets are 9-byte packets made up of a status byte, then 4 bytes for latitude, and 4 bytes for longitude. Thus, it's important to note that this tracking system does not send as much data as we seek.

Regarding the Tech Stack, the GPS transmitter is in C++ (Arduino), the Ground Station is a nodeJS app, and the client app is in JavaScript. All the communication with the XBee radio is made through serialport and, for that, the libraries serialport and xbee-api were used by the ground station to help set up the radio's config and receive data.

Lastly, the ground station has an event-driven architecture, based on events from the above mentioned libraries, such as the opening of the serialport to setup the XBee device, and the reception of new messages. This data is then parsed and sent to the Firestore DB which will then be accessed by the Client App.

---

## 2 - White Vest Model Rocket

Project for collecting, logging, emitting, and visualizing telemetry data from a model rocket containing an **inboard Raspberry Pi Zero** with an **Arduino receiving telemetry**. As opposed to the previous example, this one uses a **LoRa radio transceiver**.

This system is more detailed and contains better documentation, which can be seen in the references [2] and [3]. The software architecture is presented below:
![White Vest's Software Architecture](assets/WhiteVest/software.png)

### Air Program

In the Sensor Reading Thread, 2 devices are accessed, the BMP388 and LSM303. To fetch the value of pressure, temperature, acceleration, and magnetic orientation. These values are added to a thread-safe queue and a thread-safe single value holder. This thread can capture about 4 sets of sensor readings per second (the BMP388 reads out data quite slowly).

A separate thread reads the thread-safe queue and logs that data to a CSV file on the device. Further, an additional thread reads that thread-safe single value and transmits that data via the LoRa transceiver as floats packed into a byte array (only the most recently read data is transmitted rather than everything from the queue to have near-real-time data).

### Ground Program

On the ground, the LoRa transceiver receives those byte arrays, unpacks them to floats, and puts the data into a thread-safe queue. Another thread reads from that queue, logs that data to a CSV file, and appends the data to a thread-safe data buffer.

A 3rd thread acts as a webserver to serve the dashboard web app, and a final thread listens for WebSocket connections to stream the contents of the data buffer as data becomes available.

### Data Transmission

The transmitted data is a simple binary sequence of doubles in the following order: unix timestamp, barometric pressure, temperature, acceleration X, acceleration Y, acceleration Z, magnetic direction X, Y and Z, latitude, longitude, GPS signal quality, number of GPS sats.

### Tech Stack

- Transmitter - Python
- Ground Station - Go (handle telemetry data), Javascript(frontend) & Arduino(Configure the LoRa transceiver)

### Relevant Libraries

For python, the [adafruit_rfm9x](https://docs.circuitpython.org/projects/rfm9x/en/latest/api.html) library provides an interface that allows sending/receiving bytes of data in long range LoRa mode.

For the arduino, the [RH_RF95.h](https://www.airspayce.com/mikem/arduino/RadioHead/classRH__RF95.html) library provides a driver to send/receive unaddressed, unreliable datagram via a LoRa transceiver. This library helps to change the transceiver configuration and to receive data from it. Furthermore, [Serial](https://www.arduino.cc/reference/en/language/functions/communication/serial/) is frequently used for the communication between the arduino board and the computer.

---

## 3 - Vehicle Tracking System

This project is not a *Rocket TRS*. Nonetheless, it consists of a secure Vehicle Tracking System that uses **XBee's wireless technology**, like the first project, and displays the results on *Google Earth*, making it very similar software-wise. This one, however, lacks the source code, so it's not as thorough. As usual, below is an image representing the interactions between the components of this system:

![Vehicle Tracking System components' interactions](assets/COTS/componentsInteractions3.png)

As you can see, the vehicle contains an Arduino UNO that communicates with a GPS device and sends the received data to the Zigbee Transmitter. On the other hand,  the ground station has an **Xbee module** connected to the PC through USB. Thus, when this module receives data, it will be sent to the computer and parsed, showing it on Google Earth in real time.

### Tech Stack

- Transmitter - Arduino
- Receiver - Arduino

### Software Flow

The operation of the entire system is best described by the following Flowchart:

![Vehicle Tracking System Flowchart](assets/COTS/softwareFlow3.png)

### Relevant Tools / Libraries

- [*XCTU* Program](https://www.digi.com/products/embedded-systems/digi-xbee/digi-xbee-tools/xctu) - Used to monitor the communication between *XBees* and list the data received from the transmitter.
- *wiring* library - Arduino library which makes IO operations much easier.
- [TinyGPS](https://www.arduino.cc/reference/en/libraries/tinygps/) - GPS NMEA parsing library
- [Xbee library](https://www.arduino.cc/reference/en/libraries/xbee-arduino-library/) - Library for talking to various wireless XBee modules from Digi.

---

## Conclusion

After analyzing various examples of Total Recovery Systems, the 3 displayed in this report were the ones I found to be more useful to learn from. Especially the first 2, since they go into more detail and include the source code.

The integration of GPS with XBee/LoRa is a valid solution for a continuous and real-time tracking system, replacing the traditional *GSM* and *GPRS* protocols, which had a higher transmission cost and complexity.

There are still decisions to be made regarding the software solution we are building and one of the main purposes of this report was to raise and help answer them. As an example, in the *Rocket Tracker* project, the ground station stores the received telemetry data in a database, making it available to multiple devices, such as smartphones. As in the other 2, the ground station uses the data directly to display information.

There are also many useful tools and libraries mentioned in this report that could be effective in our software, such as the *XCTU* program. Furthermore, I've included ground station implementations with an event-driven architecture (*Rocket Tracker*)  and thread-based (*White Vest*) as they are both valid options to be considered. Certainly, event-based concurrency is also an option to have the best of both worlds.

Finally, regarding the *Tech Stack*, there are various options for both the Transmitter and Receiver. For the ground station, this report includes solutions using *Node.js*, *Go* along with *Arduino* for the configuration of the XBee module and *Arduino*. However, there are certainly more possibilities to take into account, such as Java or C++, but to make the right decision, I believe we need more information, such as how optimized our program must be and how the telemetry data will be displayed.

## References

\[1\] [Rocket Tracker Project](https://github.com/lectroidmarc/rocket-tracker)

\[2\] [White Vest GitHub](https://github.com/johnjones4/white-vest)

\[3\] [White Vest Documentation](https://johnjonesfour.com/2020/10/03/model-rocket-telemetry-part-1/)

\[4\] [Vehicle Tracking System](https://arxiv.org/ftp/arxiv/papers/1805/1805.00510.pdf)

\[5\] Arduino Documentation

<br>
<div style="text-align: right">Published by João Mesquita</div>
