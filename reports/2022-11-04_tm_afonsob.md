# SITUATION REPORT 1 (04/11/2022)

## Study White Vest
The goal of this report is to understand what are the White Vests features  and how it works, and then to adapt it to work with eggtimer.
### Introduction

This project is divided into 2 parts the **Air** and the **Ground**.

![white vests](assets/WhiteVest/software.png)
The **Air** is the system that is in the rocket itself (on-board computer), this part is the one that handles reading the sensors, logging, and sending the data to the **Ground Station**. In our case the **Air** will be the EggTimer itself.
The Ground Station has the task of receiving and storing the data sent by the rocket and allows you to view it through a web application (made with go).

### Project description
Although our goal is not to use the exact hardware they used in the White Vest. I found it important to study their implementation further to understand how to adapt it to our case.
#### Hardware
They use a Raspberry Pi Zero W as their on-board computer. And the following sensors:
-   [Adafruit RFM95W LoRa Radio Transceiver Breakout - 868 or 915 MHz - RadioFruit](https://www.adafruit.com/product/3072) 
-   [Adafruit BMP388 - Precision Barometric Pressure and Altimeter](https://www.adafruit.com/product/3966) 
-   [Zero Spy Camera for Raspberry Pi Zero](https://www.adafruit.com/product/3508) (Acho que não está previsto usarmos este)
-   [Triple-axis Accelerometer+Magnetometer (Compass) Board - LSM303](https://www.adafruit.com/product/1120) 
-   [GPS Module](https://www.amazon.com/gp/product/B084MK8BS2/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&psc=1) 

In our case the sensors will be from the EggTimer itself, so the implementation will vary slightly.

At the ground station, they use an [Arduino Uno Rev3](https://store.arduino.cc/usa/arduino-uno-rev3) and use - [Adafruit RFM95W LoRa Radio Transceiver Breakout - 868 or 915 MHz - RadioFruit](https://www.adafruit.com/product/3072) as the receiver.
We will use **Eggfinder RX Receiver** instead of the RFM95W.


####  White Vest Transmission and Logging 
The air software logs all sensor readings to a timestamped CSV file and transmits them using a LoRA transceiver, and data logging cuts off after 30 minutes. The transmitted data is a simple binary sequence of doubles in the following order:
-   Unix Timestamp
-   Barometric Pressure (Pascals)
-   Temperature (Celsius)
-   Acceleration X (M/s/s)
-   Acceleration Y (M/s/s)
-   Acceleration Z (M/s/s)
-   Magnetic Direction X (Degrees)
-   Magnetic Direction Y (Degrees)
-   Magnetic Direction Z (Degrees)
-   Latitude
-   Longitude
-   GPS Signal Quality
-   Number of GPS stat


####  How does White Vest ground receives data
The Arduino receives the data, encodes it in base64, and sends it to the dashboard through a serial port. The data is sent in the following format `T,DATA_BASE64,RSII_BASE64`.  

### DashBoard
The white vests dashboard shows the altitude, speed, temperature, pressure, pitch, yaw, signal info, and a map of the rocket's location. I think it shows all the information we need for our project, but if more is needed it doesn't seem too difficult to add.

![dashboard](assets/WhiteVest/dashboard.png)

### White Vests Problems
Some of the problems that the devs of this project pointed out, in our case don't apply:
* He pointed out problems with capturing images, but we don't need this functionality. 
* The other problem was that to increase the data capture rate we could rewrite `air.py` in c++, but since we are not going to use their module in the onboard computer, only on the ground I don't think there will be any problems.
* One improvement we can add to the dashboard is that between launches you have to manually restart the ground computer, and we can create an option to automatically create a new session without having to restart the system.

## How eggtimer sends data
The document that explains how eggtimer transmits the data is in the references of this document, point 3 ([Eggtimer Telemetry Data Format Specification](http://eggtimerrocketry.com/wp-content/uploads/2021/05/Eggtimer-Telemetry-Data-Format.pdf))
The data sent by eggtimer depends on the model we are using, as shown in the following table.
![eggtimer data](assets/COTS/eggtimer_data.png)

(I haven't had much time to understand how it works, I will do that in the next few weeks.)


## Conclusion and Next Steps
After reviewing the White Vests project, we can conclude that it seems like a very interesting starting point, since the dashboard is complete and because the project itself is quite similar to ours. But we still have one problem which is how to convert the eggtimer data to the format that white vest receives, but nothing that can't be solved. 
So the next task will be just that, to better study the format of the data that eggtimer sends and convert the data received from eggtimer to the format that White Vest receives.


## References

* \[1\] [White Vest github](https://github.com/johnjones4/white-vest)
* \[2\] [johnjonesfour](https://johnjonesfour.com/2020/10/03/model-rocket-telemetry-part-1/)
* \[3\] [Eggtimer Telemetry Data Format Specification](http://eggtimerrocketry.com/wp-content/uploads/2021/05/Eggtimer-Telemetry-Data-Format.pdf)
