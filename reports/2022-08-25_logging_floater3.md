SOFTWARE ENGINEERING DEPARTMENT

SITUATION REPORT 2

Rocket Logging System Architecture

Luís Jesus

August, 2022

Summary
=======

The main purpose of this report is to develop the first plan of our
rocket's logging system in order to get a more detailed plan of the next
tasks that will be done. In this document, we will be talking about our
overall electronic architecture and some of the possible approaches that
we may want to implement in order to get the data from the sensors
chosen by the Electrical Engineering Department.

Data will be received in two different ways: it will be stored in
comma-separated values (CSV) format in a persistent storage device, such
as a microSD card, and we'll also need to implement a "real-time\"
transmission of the data using both radio connection (main real-time
system) and 4G/5G broadband network. The viability of this last one is
still under analysis of the Electrical Engineering Team.

Velocity, GPS, altitude, accelerometer, gyroscope and magnetometer are
the parameters that we want to save and transmit.

Introduction
============

The Electrical Engineering Department was responsible for selecting the
most suitable components. We will be working with an ESP32 main board.
This microcontroller is not very energy demanding and has a quite good
configuration for the purpose we want it to.

Along with this board, we'll have five main modules/sensors that will be
responsible for retrieving information to the ESP32. We will be using a
BMP280 barometer that communicates either through SPI or I2C, a BNO055
IMU module communicating through I2C that integrates an accelerometer, a
gyroscope and a magnetometer, the Ultimate GPS Breakout v3, that will
also be responsible for velocity tracking and connects via UART and a
LoRa or XBee antenna module (yet to be decided).

Backup Logging System (microSD card)
====================================

As mentioned in the introduction, we are planning to implement a
real-time transmission of the data acquired from the sensors. However,
radio waves are very prone to be reflected by the soil, at the
beggining, and there may be some interference caused by other factors
that we may not be able to predict very accurately.

Considering this, we firstly have to think about a more reliable Logging
System that is going to be able to save all the important information in
a persistent storage device, so we can recover the data that was lost as
soon as we have the rocket "in hands\" again.

The most reasonable way to do this is by implementing a microSD Card
Module in the circuit; it usually communicates via SPI communication
protocol (the electronic implementation of this module was not
considered by the Electrical Engineering Team; they must confirm if it
can be made considering all the other modules that will be connected).

ESP32 allows us to use Arduino IDE(C/C++ languages). After some quick
research, I noticed that we will have to use SD.h and SPI.h libraries in
order to write to the SD card. Beyond these, all the sensors/modules we
will be using have their own libraries: Adafruit\_BMP280.h,
Adafruit\_BNO055.h and Adafruit\_GPS.h.

It's not very easy to start trying those implementations without having
the components. I think it is important to begin by a very simple
Arduino IDE code (without considering possible optimizations) to
analyse, for example, how much time it takes to get the data from all
the sensors and write the string in the .csv file. The first approach
must be a loop that reads sensors' values at a fixed rate, builds a
(C-)string and writes it on the file. The frequency of reporting is
actually quite variable in different projects and I couldn't find a
consensus, but it would be interesting if we started testing at a rate
of 10 logs per second (100ms). The most amount of data per second, the
better, however, we have to pay attention to some details: SD.h library
automatically writes and updates the file of the SD card in chunks of
512 bytes (it has its own buffer). The buffer is written to the card
whenever .flush() or .close() is performed or when it actually gets
full. Writing in chunks is generally better because we won't need to
open and close the file every time we want to write (and this consumes a
considerable amount of time). Considering this, if we rely on the
library and the card controller to manage the timing of the physical
storage, we'll have new data being saved every time the buffer gets
totally filled, so, if we get sensors' information every 100ms, and if
each line of the file is about 50 bytes, we will have 10 logs being
effectively appended to the file every one second. Is this enough?

Another way of doing this would be opening and closing the file in each
iteration, which means that any placement of data into the buffer, in
each iteration, would be successfully written. However, this can lead to
a bad performance and we don't want a big delay between all those
operations. It's a matter of testing and seeing which option is the
best.

Maybe the best solution is the buffer approach. We need to test if the
native SD.h library 512 bytes buffer is reliable enough, otherwise, we
may want to work on our own buffer with a custom size, until we find the
best balance between writing speed and data safety.

Logging Using Telecommunications
================================

The team is supposed to use a LoRa or a XBee antenna to send the exact
same data to the ground station. In order to implement this, we will
need to configure both the transmitter, to send data, and the receiver,
that will be on the ground station.

We want this transmission of data to be as quick as possible because we
want to have live updates of the values we will be tracking. It would
probably be enough/satisfying if we could send a new csv string to the
ground station every 100ms as well. However, the velocity of
transmission will depend on the distance between the receiver and the
transmitter, which means that at a certain altitude we may face a big
decrease on the transmission of data, which is not desirable, but I
actually didn't find clear information about this and the Electrical
Engineering Team is still deciding the best antennas to use. With more
information and knowing the boundaries that assure us a stable and
constant transmission, it will be possible to decide a suitable
configuration.

Those articles show a C implementation of communication between two
Arduino or ESP32 using Lora and its own library; they can be a good
start point. [ESP32 with LoRa using Arduino IDE -- Getting
Started](https://randomnerdtutorials.com/esp32-lora-rfm95-transceiver-arduino-ide)
and [Introduction to Lora -- Send data between two Arduino using
Lora](https://www.electronics-lab.com/project/introduction-lora-send-data-two-arduino-using-lora/).

Veredict
========

We need to test some basic implementations of Backup Logging System and
start benchmarking at different rates of data acquisition to check if
there's significant delay between reading from the sensors and writing
to the file, in order to have a stable logging system. This is very
important to make sure that the existing libraries satisfy our needs,
otherwise, we may need to compact libraries or improve an existing one
to get better results.

When it comes to the telecommunications, it is still difficult to make a
deeper study of implementation. We can actually put two microcontrollers
communicating with each other but, apart from testing (which is very
important), there is still lack of information from other departments
such as the position of the antenna(s), if it is going to be a Lora or a
XBee, how strong the connection is gonna be and how it is going to vary,
etc. This is a crucial information as it simplifies our research on the
most suitable libraries and functionalities.

Logging system will have to work flawlessly and coherently: we need to
make sure that the lag between the logs obtained by these two different
ways is minimal. This coherence and "parallelism\" between the Backup
Logging System and the Real-Time Logging System is a little bit hard to
predict without trying and testing.

A research about 4G/5G broadband networking data transmission and its
implementation along with the other two would be interesting, but it's
still not decided if we're actually going to use this (it depends on the
decision of the Electrical Engineering Department).

References
==========

\[1\] Arduino Forum;

\[2\] [Arduino Rocket Data
Logger](https://www.instructables.com/Arduino-Rocket-Data-Logger/).

\[3\] [ESP32 with LoRa using Arduino IDE -- Getting
Started](https://randomnerdtutorials.com/esp32-lora-rfm95-transceiver-arduino-ide);

\[4\] [Introduction to Lora -- Send data between two Arduino using
Lora](https://www.electronics-lab.com/project/introduction-lora-send-data-two-arduino-using-lora/);
