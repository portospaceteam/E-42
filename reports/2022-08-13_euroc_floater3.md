# SISTEMA DE REGISTO E TRACKING DE ALTITUDE

> EuRoC Design, Test & Evaluation Guide

Todos os veículos têm, obrigatoriamente, de incluir – pelo menos nos stages superiores – o Eggtimer TRS Flight Computer para medir altitude e fazer tracking GPS, uma vez que o apogeu alcançado será, oficialmente, determinado por esta via. Outros stages também requerem, obrigatoriamente, o Eggfinder GPS tracking device, mas neste caso, não é exigido que o TRS seja o fligh computer principal.

- O TRS Flight Computer é vantajoso por ser barato e por cumprir os requisitos mínimos da organização, porém, pode não ser suficiente em termos de funcionalidades e especificações para os nossos objetivos. É importante pensar se se justifica implementar um flight computer secundário caso queiramos ter mais precisão e recolher informações mais detalhadas para estudos posteriores.

- O recomendado é adquirir a versão do TRS US “Ham” (a versão EU só possui três frequências diferentes). No website em questão está indicado que a aquisição do mesmo obriga a ter uma licença FCC Amateur (Ham), ou equivalente, ainda que, durante a competição, esta seja dispensada. **É preciso avaliar se, no âmbito da competição e a partir da organização, é possível obter esta versão, uma vez que a licença não parece muito fácil de obter (pelo menos em tempo útil). Caso contrário, temos a versão EU de livre acesso mas que possui limitações consideráveis.**

- A EuRoC vai atribuir frequências diferentes a equipas diferentes, em todos os dispositivos/stages em que isso seja necessário e justificável. Apesar de tudo, devemos estar preparados porque poderá ser necessário reprogramar a frequência rapidamente.

- É obrigatório correr o firmware para a versão Ham 70cm, com seleção de canal 25kHz, de modo a ser possível selecionar as frequências atribuídas pela EuRoC.  Firmware disponível em: <http://eggtimerrocketry.com/eggfinder-lcd-receiver-support/>

- É recomendado adquirir o kit completo, que inclui LCD GPS receiver.

- É importante que todas as conexões e switches estejam localizadas num local de fácil acesso, idealmente ao nível do solo, garantindo, igualmente, que os mesmos estão seguros e protegidos durante o voo.
