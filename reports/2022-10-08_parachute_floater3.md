SOFTWARE ENGINEERING DEPARTMENT

SITUATION REPORT 3

Implementing a Parachute Launching System using Pixhawk

Luís Jesus

October, 2022

Summary
=======

With this report, I want to introduce a way to implement our Parachute
Recovery System that will guarantee that the rocket lands on the ground
successfully. This implementation was suggested by Professor Anuj Regmi
(SYSTEC). Even though there are not rocketry implementations of its
suggestion (besides BPS.space) in which we can rely on, it is relevant
to search how it could be done and some of its constraints.

As concluded in the previous meeting, designing our own board computer
"from scratch\" is actually very risky and may lead to difficult
challenges that we may not be able to solve "in-time\", which means that
we must choose a more reliable path, at least for now. The suggestion
was to use a Pixhawk Autopilot Board - Pixhawk \[[full specifications'
sheet
here](https://docs.px4.io/main/en/flight_controller/pixhawk4.html)\] -
and ArduPilot as the controller/receiver of this board (it is tipically
the software used along with Pixhawk).

If we move on to this solution, that means that a great part of the plan
made by the Electrical Engineering Department will have to be discarded,
as Pixhawk4 has in-built sensors and they won't have to face the
expected assembly and testing procedure that was initially designed.

Introduction
============

Ardupilot is a open source autopilot system that supports many vehicle
types, such as multi-copters, traditional helicopters, fixed wing
aircraft, etc. However, there is not much information about its
implementation in rockets, and the only example we have is
[BPS.SPACE](https://bps.space/), from which we don't have many
information.

Considering this, we know that this implementation would need some
adjustments to fit in the rocketry field.

Mission Planner is part of ArduPilot and it has the interface that
allows us to follow the vehicle during its flight. However, if we look
to the picture bellow, we can also see that there is not compatibility
with a rocket-type vehicle. A amateur SpaceX inspired rocket ([see
here](https://hackaday.io/project/174329-spacex-inspired-edf-rocket))
actually implemented Pixhawk running with ArduPilot V4.0.4 configured as
a single-copter, however, the mentioned project is very different. It's
not a real rocket; it \"flies\" like a drone, it does not reach high
altitude, it is remote controlled, etc. The analysis about the
requirements to make a Pixhawk based board fit a rocket model is in
charge of our team member João Gil, though.

Considering this, there is not information about parachute deployment
using Pixhawk + Ardupilot on rocketry, but we can see a more \"typical\"
case (for example, how it is used in drones).

![image](assets/Pixhawk/missionplanner.png)

Parachute Deployment System
===========================

As a baseline requirement to implement this, we have, of course, a
complete parachute mechanism like [SkyCat](https://www.skycat.pro/shop/)
or [CAD Drones](http://store.caddrones.com/products) but adapted, in
this case, to fit our own vehicle that is way larger than the vehicles
Ardupilot is meant for. This study is certainly under the analysis of
another department, as it is not related to software.

The release of the parachute is actually a mechanical \"phenomenon\" and
that made the interpretation of the documentation a little bit hard.
Maybe we need the help of a member of the Electrical and Mechanical
Departments to help us understanding what is actually happening during
the deployment of the parachute.

The parachute release mechanism can be triggered from either a Relay or
a PWM (Pulse-width modulation) signal. If a Relay pin is used, the
parachute release has to be activated on a high level and RELAY\_DEFAULT
has to be OFF or No Change, in order to avoid triggering the release
during initialization. For more information about this and other
parameters, see [Complete Parameter
List](https://ardupilot.org/plane/docs/parameters.html#relay-default)
(we need the help of other departments to better understand them).

We may want the parachute to deploy automatically or manually, but in
our case, it's just going to be automatic because the Ground Station is
not sending any kind of signals/data/information to the rocket, it will
only work as a receiver.

In the [official documentation of
ArduPilot](https://ardupilot.org/copter/docs/common-parachute.html), it
is referred that the configuration is made via Mission Planner and we
have a bunch of parameters to deal with according to our needs and
specificities. There is some information about the PINs configuration,
however, I did not understand how our rocket is in terms of mechanical
components in order to make a detailed report about the implementation
of this functionality. Considering this noticeable trouble on
understanding other technical information in the documentation, we may
need to establish a very close relationship with other departments
because there are many subjects crossing our theoretical implementation.
This terms and configurations are important to discuss with other
departments:

![image](assets/Pixhawk/mpsetup.png)

Apart from this, there is a project called
[ArduRocket](https://diydrones.com/forum/topics/ardurocket?xg_source=activity)
that seems to be a modified version of ArduPilot but it is quite old
(check [GitHub repository](https://github.com/stew-lilley/ArduRocket)).

Veredict
========

There is basically no information about Pixhawk + Ardupilot usage in
rocketry, but only about their typical implementations in other
vehicles. This means that there is a need to study the best way to make
this type of implementation compatible.

For this, it seems that the help and monitoring of other departments
(Electrical and/or Mechanical ?) is crucial to understand the viability
of the implementation. Invictus is being designed to reach an apogee of
3000 meters and it uses hybrid-propulsion; it seems quite complex when
compared to those simple vehicles supported by ArduPilot which means
that there is a big analysis of viability to be made by a competent
department before any kind of analysis of implementation.

Apart from this, if the transposition of this technology to our rocket
is achievable and viable, there is the official documentation mentioned
in this report about the setup of the parachute deployment system, that
has to be analyzed carefully not only by our department, but
fundamentally with the help of the Mechanical Engineering Department and
Electrical Engineering Department.

References
==========

\[1\] [ArduPilot](https://ardupilot.org/);

\[2\] [ArduRocket (DIY DRONES
Forum](https://diydrones.com/forum/topics/ardurocket?xg_source=activity);

\[3\] [ArduRocket GitHub
repository](https://github.com/stew-lilley/ArduRocket);

\[4\]
[Hackaday](https://hackaday.io/project/174329-spacex-inspired-edf-rocket).
