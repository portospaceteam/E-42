## SITUATION REPORT 3 (31/10/2022)

# How we can introduce a Rocket Model to a Pixhawk-based board

## Introduction

This report aims to study the viability of using a *Pixhawk* board as the central controller of a model rocket. The system must be able to capture relevant data from sensors, send telemetry information to a ground station, store logs in persistent memory, and trigger parachute systems.

---

## High-level perspective of a Flight Stack

To understand the requirements of the model rocket system, I'll present an overview of a typical flight system for drones, since the architecture is very similar.

![System Overview 1](assets/Pixhawk/systemOverview1.png)

Fig. 1 - System Overview

In the image above, to translate the system to the context of a rocket, the only component that differs is the **Drone Hardware**. This component represents the elementary drone components, such as the battery, the motors, and the GPS. Thankfully, some of these elementary components are common to both drones and rockets.

### Drone Section

Now focusing on the Drone section:

![System Overview 1](assets/Pixhawk/droneOverview.png)

Fig. 2 - Drone Overview

As we can see, above the Drone hardware is the *Flight Controller Hardware*. This is the responsibility of the **Pixhawk Board**, which is the object of study. Focusing on the *Pixhawk*, the sensors and components from the layer below (GPS, telemetry modules, etc.) hook into it, forming an essential **hardware hub**. This hub is then used by the *Software layer* to control the drones' basic components.

Regarding the top layer, the most common *Flight Controller Software* are:

1. *Ardupilot*
   - Oldest software
   - More examples are using this software
   - Operates under the GPL license, i.e any changes made to the source code are required to be pushed back to the master branch
2. *PX4*
   - Created based on *Ardupilot*
   - Feature-wise they seem to equivalent
   - Official documentation available.
   - Operates under the BSD license, i.e changes made to the source code don't need to be updated to the master branch

For our system, any of the two software should work. Based on the official docs, I prefer the *PX4* organization.

### Simulation

Thankfully, it is possible to test the *Flight Controller Software* without acquiring the hardware, through *STL (Software In The Loop)*.

![Drone STL](assets/Pixhawk/droneSTL.png)

Fig. 3 - STL Simulation

### Ground Control Station

Now focusing on the Ground Control Station:

![Ground Control Station](assets/Pixhawk/groundStationOverview.png)

Fig. 4 - Ground Control Station

In terms of hardware, the ground station just needs a telemetry module, identical to the USB module represented in the figure, that will communicate with the antenna in the rocket, using the *MAVLINK* protocol.

In the software layer, resides the user interfaces. The software represented here are essentially GUIs that utilize the *MAVLINK* protocol. Namely, *QGroundControl*, *APM Planner* and *Mission Planner*. The first 2 are for Linux, while the last one is exclusively for Windows.

Additionally, this layer also contains packages that can control vehicles, with the *MAVLINK* protocol through a coding language. In other words, these packages allow you to treat *Ardupilot* and *PX4* as an API to control the vehicle. Here we have represented the package **DroneKit**.

---

## PX4 Deep Dive

As mentioned above, PX4 is one of the most popular *Flight Controller Software*. For that reason, we will explore it in more detail since its supported features are very similar to *Ardupilot* and the documentation is easier to follow.

### Supported Vehicles

Although PX4 supports a high variety of vehicles, model rockets are not part of that list. Nonetheless, I believe that is not an issue since this list is targeted for the usage of the software as a *Professional Autopilot*, that is, controlling the vehicle's trajectory and moving it as desired. In our use case, that feature isn't required so it should be fine as long as the features mentioned in the [Introduction](##Introduction) are supported.

### System Architecture

The *PX4* documentation provides a high-level overview of a typical PX4 system based around a flight controller.

 ![PX4 System Architecture](assets/Pixhawk/px4SimpleDiagram.png)

Fig. 5 - PX4 System Architecture

Note that this architecture is similar to what we wish to achieve, as it contains most of the sensors required for the model rocket. The missing pieces in the diagram are the [*parachute triggering*](https://docs.px4.io/main/en/peripherals/parachute.html) and [*persistent storage*](https://docs.px4.io/main/en/dev_log/logging.html), which are also supported and documented.

### Flight Controllers

There are many *Pixhawk* boards to choose from, such as the [Pixhawk 4](https://docs.px4.io/main/en/flight_controller/pixhawk4.html). The comparison and analysis of the most suitable board should be made in collaboration with the *Electrical Engineering Department*.

### Supported Sensors

It is confirmed that the Pixhawk + PX4 combination supports the sensors and radio systems we are looking for. Anyhow, there are a lot of options to choose from and, once again, contact with the *Electrical Engineering Department* is needed to reach a consensus.

---

## Conclusion and Next Steps

After analyzing this information, *Pixhawk* seems to be a promising board to introduce in our model rocket. However, the current information isn't enough to reach a conclusion. To do so, we must convert this theoretical information into a practical example. Thankfully, the *Software in the Loop* solution mentioned in this report provides a way to simulate the *Pixhawk* and the rocket model. Thus, the next steps involve testing this *PX4* and *Ardupilot* software, to ensure they fit our needs and are capable of adapting to our scenario.

## References

\[1\] [Drone Programming Course](https://www.youtube.com/playlist?list=PLgiealSjeVyx3t4N9GroE29SbVwhYrOtL)

\[2\] [PX4 User Guide](https://docs.px4.io/main/en/)

\[3\] [Ardupilot](https://ardupilot.org/)

<br>
<div style="text-align: right">Published by João Mesquita</div>
