# Relate Savoir with Pixhawk

## Introduction
This report aims to analyze if it is possible to implement the savoir architecture [savoir](https://savoir.estec.esa.int/) using pixhawk as the on-board computer.


## Savoir

SAVOIR means Space Avionics Open Interface aRchitecture.
The goal of savoir is to specify a standard architecture that can be used and adapted easily for avionics related projects and thus reduce the time and risk of developing those projects


### Functional Architecture
The architecture that the savoir specifies is as follows:
![Functional Architecture](assets/Savoir/functional_architecture.png)

In our case it is simpler since we don't have payload, telecommand, neither security.


### Platform Telemetry
This module is responsible for the rocket's telemetry and is the one that sends the data to the ground station.

Note that in the SAVOIR architecture the Platform Telemetry function only directly receives data from rocket platform functions located inside the OBC, i.e. Processing, Platform Data Storage and Essential Telemetry.

In SAVOIR it plans to have 2 telemetry functions for the platform and payload, but since we don't have payload we can ignore that part.

The most commonly used configuration is the following, where we have 2 TM enconders and 2 Transmitters, with the 2 encoders connected to both transmitters.

![](assets/Savoir//TM_configuration.png)


But there is another option mainly used in telecom missions that seems to be more appropriate for our case, which is described in (SAVOIR-HB-002).
In this concept there is only 1 TM Encoder and 2 RF Links, and therefore the same data is sent in parallel to the 2 transponders.

The pixhawk has 2 telemetry ports (3 and 5).

![Pixhawk Ports](assets/Savoir/pixhawk_ports.png)


### Essential TM
The Essential Telemetry is an optional SAVOIR function manages the acquisition of telemetry needed to operate the rocket in case the Central Software is not operational. Thus it is a function that must implemented without the support of the Central Software. 
The acquired TM data are formatted into packets and sent to the Platform Telemetry where it is downlinked using a dedicated virtual channel.

In some missions the Essential Telemetry function is not required, the main argument being that in case the Central Software is not running the spacecraft mission is anyway lost. In our case I think it makes sense to have this feature, since even if the mission fails in the end we have to be able to locate and recover the rocket.

In our case this cannot be implemented since we cannot access the sensors in the pixhawk without the pixhawk's own processor. 


### Processing
The processing function is the most important since this is what manages and controls most of the functionality.
The hardware building blocks of the Processing functions are:
* the CPUs
* memories 
* FDIR support


#### The CPUs
The CPU runs the central software of the rocket, it can be just one program, or a set of applications. Pixhawk runs NuttX OS which is a Real Time OS that manages the execution of the various tasks it has to perform. 
    
According to savoir, it is also possible to split the I/O handling and central software execution between two CPUs, which is exactly what pixhawk does.
    
    
#### Memories 
* Boot Memory 
    Non-volatile memory used to store the boot software which can either run from this memory directly or load itself to the software working memory to increase speed. The memory is write protected to rule out unintentional modification. 
    
* Software Storage
    Non-volatile memory used to store the central software images and configuration data/parameters. The memory is write protected by default to avoid unintentional modification, but the write protection can be turned off using CPDU commands. 
    The pixhawk has 2MB of memory, has write protection as the savoir specifies, you need to use a debug mode. It also has support for using a Micro-SD to increase the memory size.
    
* Software Working Memory
    Volatile memory used to run the central software, which is loaded here and started by the Initialisation Sequence. This memory contains code, constants, variables, heap, etc.
    Pixhawk has 512KB RAM for the Main FMU Processor and 8KB SRAM for the IO Processor

Pixhawk also has FRAM and EEPROM used to store configuration and calibration data.

    
#### FDIR support

The processing function implements FDIR (Fault Detection, Isolation and Recovery) support functionality that operates together with the Reconfiguration Function to implement the OBC FDIR management. 
* Watchdog
    The central software implements refreshing of a watchdog signal that is monitored by the Reconfiguration function. If the refreshing stops due to hardware or software failure this triggers an alarm.
    This can be implemented with tasks in pixhawk.
* SW alarm
    The central software can also itself evaluate a situation as erroneous and trigger an alarm to hand over the responsibility of handling the error to the Reconfiguration function.
    This can be implemented with tasks in pixhawk.
* Hardware alarms
    There are several situations detected by hardware that could trigger an alarm event such as under voltage, illegal addressing from the CPU and uncorrectable memory error (including CPU registry files). 
    I don't think we have the support in pixhawk to do this
* Death report generation
    The central software may implement functionality that delivers (if possible) a death report to the Safe Guard Memory function when a reconfiguration is about to happen; the Reconfiguration function typically delays the reconfiguration to allow time for this. The death report contains the current state of the processing function, sometimes including software context, which is vital for offline error analysis.


### On-board Time and Time Reference
SAVOIR provides 2 functions related to on-Board Time.
* On-Board Time(OBT) is what keeps the current time and is where the other components of the system get their time from
* Time Reference keeps the board time synchronized (in our case the time will be synchronized with the gps time).

In SAVOIR, there are 2 OBTs the master and the Slave, if the master fails the Slave becomes the Master. It is important that if this happens that there is not too big a time jump, so it is important that the Slave is correlated with the Time Reference.

In pixhawk, the flight controller's time is set from the GPS by default [[2](https://ardupilot.org/dev/docs/ros-timesync.html)], but it doesn't seem to have redundancy in OBT.
    

### Platform Data Storage

The Platform Data Storage (PFDS) shall provide enough memory resources to enable storage of the large and various amounts of platform data:
* All TM data generated onboard 
* Mission Time Line
* On-board Control Procedures 
* Application data: configuration data, calibration data, result files 
* binary code
* System Log
* Death reports 

#### TM Packet Stores
Packet Stores can be organised in three ways:
* Circular Packet Store
    The buffer is used to store TM Packets in a circular way, works like in FIFO. If there is no space to store a new packet a new packet store has to be initialized.
* Circular Packet Store with Overwrite
    It is the same as above, but in case there is no more space he overwrites the older ones.
* Random Access Packet Store
    The buffer is used as a linear memory area with random access.

In our case, the first option seems to be the best, since we know a priori the expected flight time.

#### Cross-strappings for TM Packets

Shows the cross-strappings for the routing of TM Packets. 
It is important that the Data Storage function is used in warm redundancy, the software running in the nominal Processing function shall store data in both the nominal and redundant PFDSs. By doing so, the stored data is not lost upon a failure of one PFDS or a failure of the TFG currently in use. 

![](assets/Savoir/TM_cross_strappingss.png)


#### Memory reliability

The radiation sensitivity of the memory devices requires an EDAC function for bit error correction and a scrubbing function which avoids the accumulation of single bit failure. 

The Mass Memory shall be protected by an EDAC function providing at least single bit error correction and double bit error detection using a Hamming Code per data word. More common are today implementations using a Reed-Solomon 
code.

Mass Memory Scrubbing is performed autonomously by the Mass Memory Controller. The scrubbing rate is typically programmable. In our case, since we are not sending a rocket to very high altitudes, the rate doesn't need to be very high.

### SafeGuard Memory
The SafeGuard Memory (SGM) is a secured memory accessible either by both nominal and redundant processors.

(??)

###  Sensors

Pixhawk has three redundancy domains: Completely isolated sensor domains with separate buses and separate power control.
It has redundant sensors on separate buses, allowing parallel and continuous operation even in the event of a hardware failure, and it is still possible to connect more sensors.


## Conclusions
It is not possible to implement the savoir architecture fully using pixhawk, but it is possible to implement a large part of it. We don't know yet if we have support for implementing a part of FDIR(hardware alarms). And we can't do the Essential TM function, although it is an optional function, in our case it could make sense to implement.

## References
1. [Savoir](https://savoir.estec.esa.int/) 
2. https://ardupilot.org/dev/docs/ros-timesync.html
3. https://github.com/pixhawk/Pixhawk-Standards/blob/master/DS-011%20Pixhawk%20Autopilot%20v5X%20Standard.pdf
4. https://docs.px4.io/main/en/flight_controller/pixhawk4.html


